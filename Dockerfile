FROM ingktds/gearman
MAINTAINER ingktds <tadashi.1027@gmail.com>

ENV WORKDIR="/root"
RUN cd $WORKDIR
RUN yum -y install gcc make git bzip2 tar patch vim less mailx
RUN git clone https://github.com/tagomoris/xbuild.git
RUN xbuild/perl-install 5.24.0 ${WORKDIR}/local/perl-5.24
ENV PATH ${WORKDIR}/local/perl-5.24/bin:$PATH
RUN cpanm Gearman::Worker \
          Gearman::Client \
          Module::Install \
          Gearman::XS::Worker \
          Gearman::XS::Client \
          AnyEvent::Gearman \
          Data::MessagePack

