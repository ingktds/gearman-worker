#!/usr/bin/env perl
use strict;
use warnings;
use Gearman::Client;
use Gearman::Task;
use Data::MessagePack;
 
my $client = Gearman::Client->new();
 
$client->job_servers( '192.168.99.100:4730' );
 
my $mp = Data::MessagePack->new();
my $args = $mp->pack(+{ msg => 'Hello World!' });
my $task = Gearman::Task->new(
    print => \$args,
    +{ uniq => '-' },
);
 
# 非同期
$client->dispatch_background($task);
 
# 同期
#$client->do_task($task);
