#!/usr/bin/env perl
use strict;
use warnings;
use Gearman::Worker;
use Data::MessagePack;
 
my $worker = Gearman::Worker->new;
$worker->job_servers(
    "$ENV{G01_PORT_4730_TCP_ADDR}:4730",
    "$ENV{G02_PORT_4730_TCP_ADDR}:4730"
);
 
my $mp = Data::MessagePack->new();
$worker->register_function(
    print => sub {
        my $self = shift;
        my $arg  = $mp->unpack($self->arg);
        for (1..12) {
            print $arg->{msg} . "[$_]", "\n";
            sleep 5;
        }
    }
);
$worker->work while 1;
 
